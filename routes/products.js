var express = require('express');
var router = express.Router();
const products = require("../controllers/product.controller.js");

/* GET users listing. */
router.post("/", products.create);
router.get("/", products.list);
router.get("/:id", products.retrieve);
router.put("/:id", products.update);
router.delete("/:id", products.delete);


module.exports = router;