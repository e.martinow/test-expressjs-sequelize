const db = require('../models');
const Op = db.Sequelize.Op;

exports.create = (req, res) => {
    let data = req.body;
    let product = {
        name: data.name,
        price: data.price,
        discount: data.discount,
        marginality: data.marginality,
    };
    db.product.create(product)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "ban"
            });
        });
};

exports.list = (req, res) => {
    db.product
        .findAndCountAll()
        .then(data => {
            res.send(data);
        });
};

exports.retrieve = (req, res) => {
    id = req.params.id;
    db.product.findByPk(id)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "ban"
            });
        });
};

exports.update = (req, res) => {
    id = req.params.id;
    let data = req.body;
    let product = {
        name: data.name,
        price: data.price,
        discount: data.discount,
        marginality: data.marginality,
    }
    db.product.findByPk(id)
        .then(record => {
            if (!record) {
                res.status(404).send({
                    message: "Not found"
                });            }
            record.update(product).then(data => {
                res.send(data);
            })
        })
        .catch((error) => {
            throw new Error(error)
        })
};

exports.delete = (req, res) => {
    id = req.params.id;
    db.product.findByPk(id)
        .then(record => {
            if (!record) {
                res.status(404).send({
                    message: "Not found"
                });
            }
            record.destroy().then(data => {
                res.send(data);
            })
        })
        .catch((error) => {
            throw new Error(error)
        })
};
