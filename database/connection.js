module.exports = {
    HOST: process.env.DB_HOST,
    PORT: process.env.POSTGRESQL_PORT,
    USER: process.env.POSTGRESQL_USERNAME,
    PASSWORD: process.env.POSTGRESQL_PASSWORD,
    DB: process.env.POSTGRESQL_DATABASE,
    dialect: "postgres",
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
    }
};
